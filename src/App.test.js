import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducer from './redux/reducers'
import {
  refreshRedux,
  refreshData,
  updateData,
  EDIT_DATA,
  EDIT_STATE,
} from './redux/actions';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { mount } from 'enzyme';
import configureMockStore from 'redux-mock-store'
import fetchMock from 'fetch-mock'
import Button from './components/Button';

configure({ adapter: new Adapter() });

const API_KEY = 'https://jsonplaceholder.typicode.com/todos/1';

const store = createStore(
  reducer,
  applyMiddleware(thunk.withExtraArgument(API_KEY)),
);

const middlewares = [thunk.withExtraArgument(API_KEY)]
const mockStore = configureMockStore(middlewares);

/* tests ---- */

describe('UI components', () => {
  test('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Provider store={store}> <App /> </Provider>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('pressing button works', () => {
    const functionReturn = 'WAS CALLED';
    const wrapper = mount(<Button title="button here" onClick={() => functionReturn } />);
    expect(wrapper.invoke('onClick')()).toBe(functionReturn);
  });

  test('starting the app auto loads data', () => {
    // Will be used to clear between tests
    store.dispatch(refreshRedux());

    // Check the test started without the data being fetched and loading false
    expect(store.getState().status.loading).toBeFalsy();

    const div = document.createElement('div');
    ReactDOM.render(<Provider store={store}> <App /> </Provider>, div);  
    ReactDOM.unmountComponentAtNode(div);


    // Check that since running the app, the data is being loaded
    expect(store.getState().status.loading).not.toBeFalsy();
  })
})


describe('redux actions', () => {
    afterEach(() => {
      fetchMock.restore()
    })

    it ('requesting data goes through middle ware, loads and returns value', () => {
      fetchMock.get(API_KEY, {
        body: { id: 3, completed: false, title: 'mock data' },
        headers: { 'content-type': 'application/json' }
      })
  
      const expectedActions = [
        { type: EDIT_STATE, payload: { loading: true } },
        { type: EDIT_DATA, payload: { id: 3, completed: false, title: 'mock data' } },
        { type: EDIT_STATE, payload: { loading: false } },
      ];
  
      const store = mockStore({ data: { id: 1, completed: true, title: 'original state'}, status: { loading: false } });
  
      return store.dispatch(refreshData()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      })
    })

    it('failure to reach network results in error', () => {
      fetchMock.get(API_KEY, {throws: 'Error'});

      const expectedActions = [
        { type: EDIT_STATE, payload: { loading: true } },
        { type: EDIT_STATE, payload: { error: true, message: 'Error' } },
        { type: EDIT_STATE, payload: { loading: false } },
      ];

      const store = mockStore({ stauts: { loading: false, error: false } });

      return store.dispatch(refreshData()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      })
    });
});

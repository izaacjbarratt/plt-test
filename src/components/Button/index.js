import React from 'react';
import "./style.css";

const Button = ({ title, onClick, status }) => {
  
  return (
    <button onClick={onClick} className="plt-test__button">{title}</button>
  );
};

export default Button;

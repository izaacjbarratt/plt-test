import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
  refreshData, updateData as updateDataAction,
} from './redux/actions';
import Button from './components/Button';
import './App.css';

// Util functions
const dataItems = [
  'id',
  'title',
  'completed',
  'userId'
]
function formatData(data) {
  if (typeof data !== 'boolean') return data;
  return (data) ? 'true' : 'false';
}
function renderData(data) {
  return dataItems.map((n) => <p key={n}>{n}: {formatData(data[n])}</p>);
}


// Main component
const App = (props) => {
  const { status, data, getData, updateData } = props;

  useEffect(() => {
    // Initial call to load from API
    getData();
  }, [getData]);

  console.log('Redux State', status, data);

  function buttonPress() {
    updateData({
      title: 'The button has been clicked',
      completed: true,
    });
  }

  return (
    <div className="App">
      <div className="plt-test__page">
        <h1 className="plt-test__title">PLT TEST</h1>
        {
          (status.loading) 
            ? <p>Loading...</p>
            : <Button onClick={buttonPress} title="Press me" />
        }
      </div>

      <div className="plt-test__details">
        { (Object.values(data).length > 0 && !status.loading) ? renderData(data) : <p>No data</p> }
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    status: state.status,
    data: state.data,    
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateData: (data) => { dispatch(updateDataAction(data)) },
    getData: () => { dispatch(refreshData()) },
  }
};

App.propTypes = {
  status: PropTypes.shape({
    loading: PropTypes.bool,
    error: PropTypes.bool,
    message: PropTypes.string,
  }).isRequired,
  data: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

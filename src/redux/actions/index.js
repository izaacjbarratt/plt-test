// Action Types 
export const EDIT_STATE = 'PLT_EDIT_STATE';
export const EDIT_DATA = 'PLT_EDIT_DATA';
export const REFRESH_REDUX = 'PLT_REFRESH_REDUX';


// STATUS ACTIONS
export function setLoadingStatus(status) {
  return {
    type: EDIT_STATE,
    payload: {
      loading: status,
    },
  }
}

// DATA ACTIONS
export function updateData(data) {
  return { type: EDIT_DATA, payload: data };
}

export function dataReceivedError(err) {
  return { type: EDIT_STATE, payload: {
    error: true,
    message: err,
  }};
}

export function refreshRedux() {
  return { type: REFRESH_REDUX };
}

export function refreshData() {
  return function (dispatch, getState, api_key) {
    dispatch(setLoadingStatus(true));
    return fetch(api_key)
      .then(data => data.json())
      .then(data => {
        if (data === undefined || data === {}) {
          throw new Error('Data not found or empty');
        } else {
          dispatch(updateData(data))
          dispatch(setLoadingStatus(false));
        }
      })
      .catch((err) => {
        dispatch(dataReceivedError(err));
        dispatch(setLoadingStatus(false));
      })
  }
}


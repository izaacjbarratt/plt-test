import {
  EDIT_STATE,
  EDIT_DATA,
  REFRESH_REDUX,
} from '../actions'

const initialState = {
  status: {
    error: false,
    loading: false,
    message : '',
  },
  data: {},
};

function todoApp(state = initialState, action) {
  switch (action.type) {
    case EDIT_STATE:
      return Object.assign(
        {},
        {
          ...state,
          status: { ...state.status, ...action.payload },
        }
      );

    case EDIT_DATA:
        return Object.assign(
          {},
          {
            ...state,
            data: { ...state.data, ...action.payload },
          },
        );

    case REFRESH_REDUX:
      return initialState;

    default:
      return state
  }
}

export default todoApp;

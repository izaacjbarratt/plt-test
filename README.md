###### This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
  
-------

## PLT TEST

### PROCESS

- Started project with create-react-app

- Added various dependencies

- Started with Redux, included thunk. Seperated between DATA and STATUS to seperate the systems settings/monitoring and the imported data I'll be using.

- Created simple function to load data from App.js, called an action that used thunk middleware to fetch

- Button component created

- Concluded with unit testing to make sure all was functional 


##### TO USE

`yarn start` to run project  
`yarn test` to run tests

